# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest

from optslope import create_extended_core_model


def test_create_rpp_model():
    model = create_extended_core_model(
        ["EDD", "EDA", "RBC", "PRK"],
        ["xu5p__D"]
    )

    assert model.reactions.get_by_id("RBC").upper_bound == 1000
    assert model.reactions.get_by_id("EX_xu5p__D_e").upper_bound == 0


def test_create_rump_model():
    model = create_extended_core_model(
        ['MEDH', 'H6PS', 'H6PI', 'H4MPTP', 'FDH'],
        ["methanol", "xu5p__D"]
    )

    assert model.reactions.get_by_id("MEDH").upper_bound == 1000
    assert model.reactions.get_by_id("EX_methanol_e").upper_bound == 0
